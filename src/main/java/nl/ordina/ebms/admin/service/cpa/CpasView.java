package nl.ordina.ebms.admin.service.cpa;

import javax.annotation.PostConstruct;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = CpasView.VIEW_NAME)
public class CpasView extends VerticalLayout implements View
{
	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "CpasView";

	public CpasView()
	{
	}

	@PostConstruct
	public void init()
	{
		Label label = new Label();
		label.setValue("CPAs");
		addComponent(label);
	}

	@Override
	public void enter(ViewChangeEvent event)
	{
		Page.getCurrent().setTitle("CPAs");
	}

}
