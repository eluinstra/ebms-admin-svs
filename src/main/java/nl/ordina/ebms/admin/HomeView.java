package nl.ordina.ebms.admin;

import javax.annotation.PostConstruct;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = HomeView.VIEW_NAME)
public class HomeView extends VerticalLayout implements View
{
	private static final long serialVersionUID = 1L;
	public static final String VIEW_NAME = "";

	public HomeView()
	{
	}

	@PostConstruct
	public void init()
	{
		Label label = new Label();
		label.setValue("Welcome to the EbMS Admin Console");
		addComponent(label);
	}

	@Override
	public void enter(ViewChangeEvent event)
	{
		Page.getCurrent().setTitle("Home");
	}

}
