package nl.ordina.ebms.admin

import scala.language.implicitConversions
import com.vaadin.ui.Button.ClickEvent
import com.vaadin.ui.Button.ClickListener
import com.vaadin.ui.MenuBar
import com.vaadin.ui.MenuBar.Command

object ImplicitConversions
{
  implicit def clickListener(f: ClickEvent => Unit) : ClickListener =
    new ClickListener
    {
      override def buttonClick(event: ClickEvent)
      {
        f(event)
      }
    }

  implicit def menuSelected(f: MenuBar#MenuItem => Unit) : Command =
		new Command
		{
		  override def menuSelected(x : MenuBar#MenuItem)
		  {
		    f(x)
		  }
		}

}